package main_package;

public class TestHarness {

	/*There is an elevator in a building with L floors. 
	This elevator can take a max of C people at a time or 
	max of total weight W. The elevator serves in “first 
	come first serve” basis and the order for the queue can 
	not be changed. This program will work out how many stops 
	the elevator has to take to serve all the people.
	Person 1 in the queue would have a weight of A[0] and a 
	destination of B[0].
	Person 2 in the queue would have a weight of A[1] and a 
	destination of B[1].
	Person 2 in the queue would have a weight of A[2] and a 
	destination of B[2]
	*/
	
	
	public static void main(String[] args)
	{
		/* A - An array of passenger weights */		
		int[] A = new int[5];
		A[0] =  60;    		
	    A[1] =  80;    
	    A[2] = 40 ;
	    A[3] =  60;    
	    A[4] = 30 ;
		
	    /* B - An array of passenger destinations */
		int[] B = new int[5];
		B[0] = 2;
	    B[1] = 3;
	    B[2] = 5;
	    B[3] = 1;
	    B[4] = 4;
	    
	    /* L - The number of levels in the building*/
	    int L = 5;
	    
	    /* C - The capacity of the lift */
	    int C = 3;
	    
	    /* W - The max weight of the lift */
	    int W = 200;
	    
	    /*The building has 5 floors, maximum of 2 persons are
	    allowed in the elevator at a time with max weight capacity 
	    being 200. For this example, the elevator would take total 
	    of 7 stops in floors: 2, 3, 5, G, 1, 4 and finally G.
	    */   
	    Elevatorstopcalculator esc = new Elevatorstopcalculator();
	    System.out.println("Number of elevator stops required is "+ esc.solution(A, B, L, C, W));
	}

}