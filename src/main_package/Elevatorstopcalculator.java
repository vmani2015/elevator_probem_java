package main_package;

import java.util.HashSet;
import java.util.Set;

public class Elevatorstopcalculator {
	
	
	/*
	* @param A - An array of passenger weights
	* @param B - Ar array of passenger levels
	* @param floors - The number of floors in the building
	* @param capacity - The maximum number of people who can fit in the elevator
	* @param elevator_weight - The maximum weight that the elevator can hold
	* @return int - The numnber of stops that the elevator makes
	*/	
	public int solution(int[] A, int[] B, int floors, int capacity, int elevator_weight)
	{
		int counter = 0;
		int current_trip = 0;
		int total_trips = 0;
		int trips_to_remove = 0;
		
		while (counter < A.length)
		{
			
				
			//Reset the current trip
			current_trip = 0;
			
			current_trip = WorkOutTrip(counter,A, capacity, elevator_weight);
						
			
			
//			Now we calculate the trips required for the same level.
		if (current_trip >= 1)
		{
			trips_to_remove = trips_to_remove + WorkOutTripsToRemove(B, counter, current_trip);
		}
		
			
			//Now we need a ground floor trip
			//Add 1 because of this
			total_trips = total_trips + current_trip + 1;
			
			//As we want the next trip
			counter = counter + current_trip ;
		}	
			
		
		
		return total_trips- trips_to_remove;
		
	}
	
	/* Method used if more than one passenger gets off at the same level
	* @param B - Ar array of passenger levels
	* @param counter - The number of times to loop
	* @param current_trip - The current position in the array
	* @return int - The number of stops to remove
	*/	
	public int WorkOutTripsToRemove(int[] B,int counter, int current_trip)
	{
		Set<Integer> s = new HashSet<Integer>();
		

		
		//Check if any of them have the same level
		for (int i= counter ; i < current_trip +counter ; i++)
		{
			s.add(B[i]);
		}
		
		if (current_trip != s.size())
		{
			return  (current_trip - s.size()); 
		}
		
		return 0;
	}
		
	
	public int WorkOutTrip(int start_index,int[] A, int capacity, int elevator_weight)
	{
		int current_weight = 0;
		int counter = 0;
	
		
		boolean from_start = true;
		
		if (start_index > 0)
		{
			from_start = false;
		}
		
		
		
		while (counter < capacity && start_index < A.length && current_weight < elevator_weight)
		{
			if (counter == A.length)
			{
				break;
			}
			else
			{
				if (from_start == false)
				{
					current_weight = current_weight +A[start_index];
				}
				else
				{
					current_weight = current_weight +A[counter];
				}
				
			}
			
			if (current_weight > elevator_weight)
			{
				break;
			}
			else
			{
				counter ++;
				if (from_start == false)
				{
					start_index ++;
				}
				
			}
		}
		
		
		return counter;
	}

}

